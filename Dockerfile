#Imagen Base
FROM ubuntu:latest

#Datos de entrada
ARG VERSION=2.0.0

#Creamos usuarios para el docker
USER root

#Instalamos Java 8
#RUN yum update -y && \
#yum install -y wget && \
#yum install -y java-1.8.0-openjdk java-1.8.0-openjdk-devel && \
#yum clean all 

# update
RUN apt-get update

# Instalacion wget
#RUN apt-get -y install wget

# Instalacion systemd
#RUN apt-get install -y systemd

# Instalacion ssh
RUN apt-get install -y openssh-client
RUN apt-get install sshpass
#RUN systemctl status ssh

#Instalacion curl
RUN apt-get update
RUN apt-get -y install curl gnupg
RUN curl -sL https://deb.nodesource.com/setup_11.x  | bash -
RUN apt-get -y install nodejs

#Cambio ruta
WORKDIR /tmp

EXPOSE 3000

#Creamos un usuario sin home y cambiamos a este
#RUN useradd --no-create-home --shell /bin/sh nonroot
#USER nonroot
